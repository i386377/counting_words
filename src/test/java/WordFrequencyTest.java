import com.example.counting_words_jakarta.IWordFrequency;
import com.example.counting_words_jakarta.IWordFrequencyAnalyzer;
import com.example.counting_words_jakarta.WordFrequencyAnalyzerImpl;
import com.example.counting_words_jakarta.WordFrequency;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class WordFrequencyTest {

    private final IWordFrequencyAnalyzer IWordFrequencyAnalyzer = new WordFrequencyAnalyzerImpl();

    @Test
    public void calculateHighestFrequencyTest(){

        String text = "test hoi test hoi hoi de ";
        int expected = 3;

        int frequency = IWordFrequencyAnalyzer.calculateHighestFrequency(text);

        Assertions.assertEquals(expected, frequency);
    }

    @Test
    public void calculateHighestFrequency_EmptyStringTest(){

        String text = "";
        int expected = 0;

        int frequency = IWordFrequencyAnalyzer.calculateHighestFrequency(text);

        Assertions.assertEquals(expected, frequency);
    }

    @Test
    public void calculateHighestFrequency_CaseInsensitiveTest(){

        String text = "test hoI tESt hOi hoi de ";
        int expected = 3;

        int frequency = IWordFrequencyAnalyzer.calculateHighestFrequency(text);

        Assertions.assertEquals(expected, frequency);
    }

    @Test
    public void calculateHighestFrequency_MultipleHighestFrequencyTest(){

        String text = "test hoI tESt hoi de ";
        int expected = 2;

        int frequency = IWordFrequencyAnalyzer.calculateHighestFrequency(text);

        Assertions.assertEquals(expected, frequency);
    }

    @Test
    public void calculateFrequencyForWord_WordExistTest(){

        String text = "Test hoi test hoi hoi de ";
        String word = "test";
        int expected = 2;


        int frequencyForWord = IWordFrequencyAnalyzer.calculateFrequencyForWord( text, word);

        Assertions.assertEquals(expected, frequencyForWord);
    }

    @Test
    public void calculateFrequencyForWord_WordNotExistTest(){

        String text = "Test hoi test hoi hoi de";
        String word = "notExist";
        int expected = 0;

        int frequencyForWord = IWordFrequencyAnalyzer.calculateFrequencyForWord( text, word);

        Assertions.assertEquals(expected, frequencyForWord);
    }

    @Test
    public void calculateFrequencyNWords_InBoundsTest(){

        String text = "Test hoi test hoi hoi de ";
        int n = 2;
        int expected = 2;
        WordFrequency wordFrequencyExpected1 = new WordFrequency("hoi", 3);
        WordFrequency wordFrequencyExpected2 = new WordFrequency("test", 2);


        List<WordFrequency> frequencyForWord = IWordFrequencyAnalyzer.calculateMostFrequentNWords( text, n);
        String word = frequencyForWord.get(0).getWord();
        int frequency = frequencyForWord.get(0).getFrequency();

        String word2 = frequencyForWord.get(1).getWord();
        int frequency2 = frequencyForWord.get(1).getFrequency();

        Assertions.assertEquals(expected, frequencyForWord.size());

        // check most word with most frequency
        Assertions.assertEquals(wordFrequencyExpected1.getWord(), word);
        Assertions.assertEquals(wordFrequencyExpected1.getFrequency(), frequency);

        // check second most word with most frequency
        Assertions.assertEquals(wordFrequencyExpected2.getWord(), word2);
        Assertions.assertEquals(wordFrequencyExpected2.getFrequency(), frequency2);
    }

    @Test
    public void calculateFrequencyNWords_OutOfBoundsTest(){

        String text = "Test hoi test hoi hoi de ";
        int n = 5;
        int expected = 3;

        List<WordFrequency> frequencyForWord = IWordFrequencyAnalyzer.calculateMostFrequentNWords( text, n);

        Assertions.assertEquals(expected, frequencyForWord.size());
    }

    @Test
    public void calculateFrequencyNWords_NZeroTest(){

        String text = "Test hoi test hoi hoi de ";
        int n = 0;
        int expected = 0;

        List<WordFrequency> frequencyForWord = IWordFrequencyAnalyzer.calculateMostFrequentNWords( text, n);

        Assertions.assertEquals(expected, frequencyForWord.size());
    }
}
