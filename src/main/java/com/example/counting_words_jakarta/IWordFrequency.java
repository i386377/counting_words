package com.example.counting_words_jakarta;

public interface IWordFrequency {

    String getWord();
    int getFrequency();
}
