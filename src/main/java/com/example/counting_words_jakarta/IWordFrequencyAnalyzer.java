package com.example.counting_words_jakarta;

import java.util.List;

public interface IWordFrequencyAnalyzer {

    int calculateHighestFrequency(String text);
    int calculateFrequencyForWord(String text, String word);
    List<WordFrequency> calculateMostFrequentNWords(String text, int n);
}
