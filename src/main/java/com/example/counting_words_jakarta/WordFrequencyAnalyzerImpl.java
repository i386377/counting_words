package com.example.counting_words_jakarta;

import java.util.*;

public class WordFrequencyAnalyzerImpl implements IWordFrequencyAnalyzer {

    @Override
    public int calculateHighestFrequency(String text) {

        //check if text exist
        if (text == null || text.isEmpty()){
            return 0;
        }

        List<WordFrequency> wordFrequenciesList = createWordFrequencyList(text);
        int maxFrequency = 0;

        // get max frequency
        for(WordFrequency wordFrequency: wordFrequenciesList){
            if(wordFrequency.getFrequency() > maxFrequency){
                maxFrequency = wordFrequency.getFrequency();
            }
        }

        return maxFrequency;
    }

    @Override
    public int calculateFrequencyForWord(String text, String word) {

        String[] wordsArray = text.toLowerCase().split("\\s+");

        int counter = 0;

        // if word is in text, increase counter
        for(String item: wordsArray){
            if(Objects.equals(item, word.toLowerCase())){
                counter ++;
            }
        }

        return counter;
    }

    @Override
    public List<WordFrequency> calculateMostFrequentNWords(String text, int n) {

        List<WordFrequency> wordFrequencyList = createWordFrequencyList(text);
        List<WordFrequency> sortedWordFrequencyList = new ArrayList<>();

        // sort list descending by frequency
        wordFrequencyList.sort(Comparator.comparingInt(WordFrequency::getFrequency).reversed());

        // limit list by n
        for(int i = 0; i< n; i++){
            if(i < wordFrequencyList.size()) {
                sortedWordFrequencyList.add(wordFrequencyList.get(i));
            }
        }

        return sortedWordFrequencyList;
    }

    private List<WordFrequency> createWordFrequencyList(String text) {
        String[] wordsArray = text.toLowerCase().split("\\s+");

        List<WordFrequency> map = new ArrayList<>();

        // increase by 1 if word exist in array.
        for(String word: wordsArray){
            if(map.stream().noneMatch(wordFrequency -> Objects.equals(wordFrequency.getWord(), word))){
                WordFrequency newWordFrequency = new WordFrequency(word, 1);
                map.add(newWordFrequency);
            } else{
                for(WordFrequency wordFrequency: map){
                    if(Objects.equals(word, wordFrequency.getWord())){
                        wordFrequency.increaseFrequency();
                    }
                }
            }
        }

        return map;
    }
}
