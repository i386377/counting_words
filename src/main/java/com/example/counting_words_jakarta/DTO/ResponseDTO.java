package com.example.counting_words_jakarta.DTO;

import com.example.counting_words_jakarta.WordFrequency;
import java.util.List;

public class ResponseDTO {

    int response;
    List<WordFrequency> wordFrequencyList;

    public ResponseDTO(int response, List<WordFrequency> wordFrequencyList) {
        this.response = response;
        this.wordFrequencyList = wordFrequencyList;
    }

    public ResponseDTO(){
    }

    public int getResponse() {
        return response;
    }

    public void setResponse(int response) {
        this.response = response;
    }

    public List<WordFrequency> getWordFrequencyList() {
        return wordFrequencyList;
    }

    public void setWordFrequencyList(List<WordFrequency> wordFrequencyList) {
        this.wordFrequencyList = wordFrequencyList;
    }
}
