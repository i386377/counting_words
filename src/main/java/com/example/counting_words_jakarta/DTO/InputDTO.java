package com.example.counting_words_jakarta.DTO;

public class InputDTO {

    private String word;
    private String text;
    private int count;

    public InputDTO(String word, String text, int count) {
        this.word = word;
        this.text = text;
        this.count = count;
    }

    public InputDTO(){
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
