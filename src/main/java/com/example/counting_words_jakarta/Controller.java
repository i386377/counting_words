package com.example.counting_words_jakarta;

import com.example.counting_words_jakarta.DTO.InputDTO;
import com.example.counting_words_jakarta.DTO.ResponseDTO;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import java.util.List;

@Path("/rest")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class Controller {

    private final IWordFrequencyAnalyzer IWordFrequencyAnalyzer = new WordFrequencyAnalyzerImpl();

    @POST
    @Path("/highest-frequency")
    public ResponseDTO calculateHighestFrequency(InputDTO inputDTO) {
        int highestFrequency = IWordFrequencyAnalyzer.calculateHighestFrequency(inputDTO.getText());
        ResponseDTO responseDTO = new ResponseDTO();
        responseDTO.setResponse(highestFrequency);

        return responseDTO;
    }

    @POST
    @Path("/frequency-word")
    public ResponseDTO calculateFrequencyForWord(InputDTO inputDTO) {
        int frequencyForWord = IWordFrequencyAnalyzer.calculateFrequencyForWord(inputDTO.getText(), inputDTO.getWord());
        ResponseDTO responseDTO = new ResponseDTO();
        responseDTO.setResponse(frequencyForWord);

        return responseDTO;
    }

    @POST
    @Path("/frequency-n-word")
    public ResponseDTO calculateMostFrequentNWords(InputDTO inputDTO) {
        List<WordFrequency> wordFrequencyList = IWordFrequencyAnalyzer.calculateMostFrequentNWords(inputDTO.getText(), inputDTO.getCount());
        ResponseDTO responseDTO = new ResponseDTO();
        responseDTO.setWordFrequencyList(wordFrequencyList);

        return responseDTO;
    }
}