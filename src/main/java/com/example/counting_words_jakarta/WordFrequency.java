package com.example.counting_words_jakarta;

public class WordFrequency implements IWordFrequency {

    private String word;
    private int frequency;

    public WordFrequency(String word, int frequency) {
        this.word = word;
        this.frequency = frequency;
    }

    public WordFrequency(){
    }

    public void setWord(String word) {
        this.word = word;
    }

    public void increaseFrequency(){
        frequency++;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public String getWord() {
        return word.toLowerCase();
    }

    public int getFrequency() {
        return frequency;
    }
}
